import App from './App'
import request from './utils/request.js'
Vue.prototype.$axios=request;

// #ifndef VUE3
import Vue from 'vue'
import uView from "uview-ui";
Vue.use(uView);
Vue.config.productionTip = false
// 全局网络检测配置 （比如未连接网络则无法进入页面）
Vue.prototype.networkDetection = function () {
  var vm = this; // 存储 Vue 实例的引用
  uni.getNetworkType({
    success: function(res) {
      console.log(res.networkType);
      if (res.networkType === 'none') {
        uni.showModal({// 没连接网 提示弹窗
          title: '提示',
          content: '请先连接网络',
          showCancel: false,
          success: function(res) {
            if (res.confirm) {
              uni.reLaunch({//点击刷新页面
                url: '/' + getCurrentPages()[0].route
              });
            }
          }
        });
      } else {
        console.log("已连接网络");
      }
    }
  });
}

Vue.prototype.URL = "http://127.0.0.1:17000/"

const globalBus = new Vue({
  data: {
    timer: null
  },
  methods: {
 startTimer() {
   this.timer = setInterval(() => {
     uni.request({
       url: 'http://127.0.0.1:17000/memberUser/selectCoachApplyByInvitationCode',
       method: "GET",
       data: {
         memberUserId:uni.getStorageSync('userInfo').id,
         invitationCode: uni.getStorageSync('userInfo').invitationCode,
         venueUniqueIdentifier: uni.getStorageSync('userInfo').venueUniqueIdentifier
       },
       success: (res) => {
         if(res.data.data.data.applyStatus === 0) {
           uni.request({
             url: 'http://127.0.0.1:17000/memberUser/selectCoachByInvitationCode',
             method: "GET",
             data: {
               invitationCode: res.data.data.data.invitationCode,
               venueUniqueIdentifier: uni.getStorageSync('userInfo').venueUniqueIdentifier
             },
             success: (res) => {
               uni.setStorageSync('coach', res.data.data.data.id);
               globalBus.stopTimer();
             }
           });
         }
       }
     });
     // 执行定时任务的逻辑
     console.log('执行定时任务');
   }, 5000);
 },
    stopTimer() {
      clearInterval(this.timer)
    },
    checkLocalStorage() {
      const coach = localStorage.getItem('coach')
      if (coach) {
        this.stopTimer()
      }
    }
  }
})

if (typeof window !== 'undefined') {
  window.addEventListener('storage', (event) => {
    if (event.key === 'coach') {
      globalBus.checkLocalStorage()
    }
  })
}

Vue.prototype.$globalBus = globalBus

App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif